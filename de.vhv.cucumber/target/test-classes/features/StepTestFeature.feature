@LoginTest1
Feature: Login1
  Durchfuehrung einer erfolgreichen Anmeldung
  Als user
  Gebe ich korrekte Logindaten ein 

 @TestPositiv
  Scenario: Anmeldung in Gmail mit korrekten Eingabedaten
    Given der user ruft die gmail-Webseite auf 
    When der user gibt den Benutzernamen und das Passwort ein  
    | Benutzername   | Passwort    |
	| tayane.mehdi01 | mohammed1986|
	| tayane_mehdi@hotmail.de | mohammed1986|
		   	
    And der user clickt auf submit button
    Then der user ist erfolgreich eingeloggt
    When der user clickt auf dropdown menu 
    And der user clickt auf logOut button
	Then der user ist ausgeloggt
	And der user befindet sich auf der Loginpage wieder

 @NegativTest
  Scenario:  Anmeldung in Gmail mit falschen Eingabedaten
    Given der user ruft die gmail-Webseite auf 
    When der user gibt den Benutzernamen und das Passwort ein  
    | Benutzername   | Passwort   |
	| tayane.mehdi01 | mhammed1986|
	| tayane_mehdi@hotmail.de | mhammed1986|
	   	
    And der user clickt auf submit button
    Then der user ist nicht eingeloggt
	And eine Fehlermeldung ist aufgetreten 
   
 @SmokeTest
 Scenario: Eine Email nach der Anmeldung in Gmail schreiben
    Given der user ruft die gmail-Webseite auf 
    When der user gibt den Benutzernamen und das Passwort ein  
    | Benutzername   | Passwort    |
	| tayane.mehdi01 | mohammed1986|
	| tayane_mehdi@hotmail.de | mohammed1986|
		   	
    And der user clickt auf submit button
    Then der user ist erfolgreich eingeloggt
    When der user auf SchreibenButton clickt
    Then oeffnet sich ein NeueNachrihtFenster 
    When der user email von dem Empfaenger und den Betreff eingibt
    | Emailadresse des Empfeangers | Betreff       |
    | mehdi.tayane86@gmail.com     | Cucumber Test |
    And der user eine Nachricht eingibt
    | neue NAchricht |
    | Hallo Zusammen |
    Then wird die Nachricht angezeigt 
    When der user clickt auf dropdown menu 
    And der user clickt auf logOut button
	Then der user ist ausgeloggt
	And der user befindet sich auf der Loginpage wieder
 @TestExample
 Scenario: Beispiel 
    Given der user ruft die gmail-Webseite auf 
    When der user gibt den Benutzernamen und das Passwort ein  
    | Benutzername   | Passwort    |
	| tayane.mehdi01 | mohammed1986|
	| tayane_mehdi@hotmail.de | mohammed1986|
	
    And der user clickt auf submit button
    Then der user ist erfolgreich eingeloggt
    When der user clickt auf dropdown menu 
    And der user clickt auf Entwürfe_Tab
	And der user befindet sich auf der entworfenen emails

 	
    
  
