@LoginTest
Feature: Login
  Durchfuehrung einer erfolgreichen Anmeldung
  Als Benutzer
  Gebe ich korrekte Logindaten ein

  @LoginTestPositiv
  Scenario: Ueberpruefung der Anmeldung in Gmail mit korrekten Eingabedaten
    Given der Benutzer ruft die gmail-Webseite auf
    When der Benutzer gibt den Benutzernamen und das Passwort ein
      | Benutzername   | Passwort     |
      | mehdi.tayane86 | Mohammed1986 |
    And der Benutzer clickt auf submit button
    Then der Benutzer ist erfolgreich eingeloggt
    When der Benutzer clickt auf dropdown menu
    And der Benutzer clickt auf logOut button
    Then der benutzer ist ausgeloggt
    And der Benutzer befindet sich auf der Loginpage wieder

  @LoginTestNegativ
  Scenario: Ueberpruefung der Anmeldung in gmail mit falschen Eingabedaten
    Given der Benutzer ruft die gmail-Webseite auf
    When der Benutzer gibt den Benutzernamen und das Passwort ein
      | Benutzername   | Passwort    |
      | mehdi.tayane86 | mhammed1986 |
    And der Benutzer clickt auf submit button
    Then der Benutzer ist nicht eingeloggt
    And ein Fehlermeldung ist aufgetreten
