$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/LoginFeature.feature");
formatter.feature({
  "line": 2,
  "name": "Login",
  "description": "Durchfuehrung einer erfolgreichen Anmeldung\nAls Benutzer\nGebe ich korrekte Logindaten ein",
  "id": "login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@LoginTest"
    }
  ]
});
formatter.before({
  "duration": 3315598731,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Ueberpruefung der Anmeldung in Gmail mit korrekten Eingabedaten",
  "description": "",
  "id": "login;ueberpruefung-der-anmeldung-in-gmail-mit-korrekten-eingabedaten",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@LoginTestPositiv"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "der Benutzer ruft die gmail-Webseite auf",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "der Benutzer gibt den Benutzernamen und das Passwort ein",
  "rows": [
    {
      "cells": [
        "Benutzername",
        "Passwort"
      ],
      "line": 11
    },
    {
      "cells": [
        "mehdi.tayane86",
        "Mohammed1986"
      ],
      "line": 12
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "der Benutzer clickt auf submit button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "der Benutzer ist erfolgreich eingeloggt",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "der Benutzer clickt auf dropdown menu",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "der Benutzer clickt auf logOut button",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "der benutzer ist ausgeloggt",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "der Benutzer befindet sich auf der Loginpage wieder",
  "keyword": "And "
});
formatter.match({
  "location": "LoginStep.der_Benutzer_ruft_die_gmail_Webseite_auf()"
});
formatter.result({
  "duration": 2818886636,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_gibt_den_Benutzernamen_und_das_Passwort_ein(DataTable)"
});
formatter.result({
  "duration": 4553643323,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_clickt_auf_submit_button()"
});
formatter.result({
  "duration": 106066791,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_ist_erfolgreich_eingeloggt()"
});
formatter.result({
  "duration": 5904023622,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_clickt_auf_dropdown_menu()"
});
formatter.result({
  "duration": 2173849234,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_clickt_auf_logOut_button()"
});
formatter.result({
  "duration": 12111053651,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//a[@class \u003d \u0027gb_0 gb_Ef gb_Mf gb_le gb_kb\u0027]\"}\n  (Session info: chrome\u003d74.0.3729.131)\n  (Driver info: chromedriver\u003d2.42.591088 (7b2b2dca23cca0862f674758c9a3933e685c27d5),platform\u003dWindows NT 10.0.16299 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.8.1\u0027, revision: \u00276e95a6684b\u0027, time: \u00272017-12-01T18:33:54.468Z\u0027\nSystem info: host: \u0027ANASUS005\u0027, ip: \u0027192.168.141.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.42.591088 (7b2b2dca23cca0..., userDataDir: C:\\Users\\TayaneMe\\AppData\\L...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:50008}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 74.0.3729.131, webStorageEnabled: true}\nSession ID: 6ffc2b8345dde6c164f1499c664e76a1\n*** Element info: {Using\u003dxpath, value\u003d//a[@class \u003d \u0027gb_0 gb_Ef gb_Mf gb_le gb_kb\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:371)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:473)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:363)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy20.click(Unknown Source)\r\n\tat steps.LoginStep.der_Benutzer_clickt_auf_logOut_button(LoginStep.java:94)\r\n\tat ✽.And der Benutzer clickt auf logOut button(src/test/resources/features/LoginFeature.feature:16)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "LoginStep.der_benutzer_ist_ausgeloggt()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_befindet_sich_auf_der_Loginpage_wieder()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1089669421,
  "status": "passed"
});
formatter.before({
  "duration": 2201863248,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Ueberpruefung der Anmeldung in gmail mit falschen Eingabedaten",
  "description": "",
  "id": "login;ueberpruefung-der-anmeldung-in-gmail-mit-falschen-eingabedaten",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 20,
      "name": "@LoginTestNegativ"
    }
  ]
});
formatter.step({
  "line": 22,
  "name": "der Benutzer ruft die gmail-Webseite auf",
  "keyword": "Given "
});
formatter.step({
  "line": 23,
  "name": "der Benutzer gibt den Benutzernamen und das Passwort ein",
  "rows": [
    {
      "cells": [
        "Benutzername",
        "Passwort"
      ],
      "line": 24
    },
    {
      "cells": [
        "mehdi.tayane86",
        "mhammed1986"
      ],
      "line": 25
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "der Benutzer clickt auf submit button",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "der Benutzer ist nicht eingeloggt",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "ein Fehlermeldung ist aufgetreten",
  "keyword": "And "
});
formatter.match({
  "location": "LoginStep.der_Benutzer_ruft_die_gmail_Webseite_auf()"
});
formatter.result({
  "duration": 2946106331,
  "status": "passed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_gibt_den_Benutzernamen_und_das_Passwort_ein(DataTable)"
});
formatter.result({
  "duration": 14477471994,
  "error_message": "org.openqa.selenium.ElementNotVisibleException: element not interactable\n  (Session info: chrome\u003d74.0.3729.131)\n  (Driver info: chromedriver\u003d2.42.591088 (7b2b2dca23cca0862f674758c9a3933e685c27d5),platform\u003dWindows NT 10.0.16299 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.8.1\u0027, revision: \u00276e95a6684b\u0027, time: \u00272017-12-01T18:33:54.468Z\u0027\nSystem info: host: \u0027ANASUS005\u0027, ip: \u0027192.168.141.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_201\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.42.591088 (7b2b2dca23cca0..., userDataDir: C:\\Users\\TayaneMe\\AppData\\L...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:50057}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 74.0.3729.131, webStorageEnabled: true}\nSession ID: 18be94d286061f0bd57c676d712473f6\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:279)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.sendKeys(RemoteWebElement.java:100)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:51)\r\n\tat com.sun.proxy.$Proxy20.sendKeys(Unknown Source)\r\n\tat steps.LoginStep.der_Benutzer_gibt_den_Benutzernamen_und_das_Passwort_ein(LoginStep.java:60)\r\n\tat ✽.When der Benutzer gibt den Benutzernamen und das Passwort ein(src/test/resources/features/LoginFeature.feature:23)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_clickt_auf_submit_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "LoginStep.der_Benutzer_ist_nicht_eingeloggt()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "LoginStep.eine_Fehlermeldung_ist_aufgetreten()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "duration": 1066802652,
  "status": "passed"
});
});