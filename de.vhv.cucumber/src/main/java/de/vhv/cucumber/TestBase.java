package de.vhv.cucumber;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestBase { 
    private WebDriver driver; 

    protected WebDriver getDriver() { 
     if (driver != null) { 
      return driver; 
     } 
     System.setProperty("webdriver.chrome.driver","./libs/chromedriver.exe"); 
     driver = new ChromeDriver(); 
     return driver; 
    } 
    public void setUP() {
    	getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void getUrl(String url) { 
     getDriver().get(url); 
     getDriver().manage().window().maximize(); 
    } 

} 
